var mongooseTimestamps = require('../'),
    mongoose = require('mongoose'),
    expect = require('expect.js'),
    MongoSchema = new mongoose.Schema();

/*
 * Add the plugin to mongoose
 */
MongoSchema.plugin(mongooseTimestamps);

/*
 * Connect to mongodb for testing after save hooks
 */
mongoose.connect('mongodb://localhost/mongoose-simple-timestamps');
mongoose.connection.on('error', function(err){
  console.error('MongoDB error:', err);
  console.error('Make sure a mongoDB server is running and accessible by this application');
});

/*
 * Create a model to test with
 */
var Model = mongoose.model('Model', MongoSchema);

describe('mongoose-simple-timestamps', function(){

  it('should add a createdAt and updatedAt date field to the schema', function(){
    expect(MongoSchema.paths).to.have.property('createdAt');
    expect(MongoSchema.paths).to.have.property('updatedAt');
  });

  it('should set both the updated and createdAt attributes on save when new', function(done){
    model = new Model();
    model.save(function(err){
      console.log(model);
      expect(model.createdAt).to.equal(model.updatedAt).and.not.to.be(undefined);
      done(err)
    });
  });

  it('should not change the createdAt when updating a document', function (done) {
    model = new Model();
    model.save(function(err){
      if (err) return done(err);
      model.save(function(err){
        expect(model.createdAt).to.not.equal(model.updatedAt).and.not.to.be(undefined);
        done(err);
      });
    });
  });

  it('should work even if you dont select the timestamps of the document', function (done) {
    model = new Model();
    model.save(function(err){
      if (err) return done(err);
      Model.findById(model.id).select('_id').exec(function(err, doc){
        if (err) return done(err);
        model.save(function(err){
          expect(model.createdAt).to.not.equal(model.updatedAt).and.not.to.be(undefined);
          done(err);
        });
      });
    });
  });

  after(function(done){
    Model.remove(function(err){
      done(err)
    });
  });
})