# mongoose-simple-timestamps

## What?

A mongoose js plugin which adds a 'createdAt' and an 'updatedAt' timestamp to your documents. The createdAt will only be set when creating and the updatedAt will be updated to the current time on each save.

## Why? 

_Why build another timestamp lib?_
1. Because timestamps are simple and you probably only need a createdAt and updatedAt timestamp.
2. Because you want a mongoose timestamp library which doesn't break when you do a select and don't select the timestamps.

_Why not use the timestamp in the mongodb id?_
Because you quite often want to query the createdAt date and have easier access to it.

## How to use it?

See [tests](./test/mongoose-simple-timestamps.js)

```javascript
    var mongoose = require('mongoose'),
        mongooseSimpleTimestamps = require('mongoose-simple-timestamps'),
        MongoSchema = new mongoose.Schema();

    MongoSchema.plugin(mongooseTimestamps);
    
    model = new Model();
    model.save(function(err){
      console.log(model); // => { __v: 0, createdAt: Mon Dec 01 2014 20:05:12 GMT+0000 (GMT), updatedAt: Mon Dec 01 2014 20:05:12 GMT+0000 (GMT), _id: 547cc9f8e3334e3b22e092dc }
    });
```
