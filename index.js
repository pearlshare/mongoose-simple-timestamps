
module.exports = function(schema, options){
  schema.add({updatedAt: Date, createdAt: Date});

  schema.pre('save', function(next){
    if (this.isNew){
      this.createdAt = this.updatedAt = new Date();
    } else {
      this.updatedAt = new Date();
    };
    next();
  })
};